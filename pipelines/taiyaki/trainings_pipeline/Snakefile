configfile: "taiyaki_config.yaml"
"""
Goal:
    Pipeline with all the steps needed to train a new taiyaki model to use for basecalling with guppy.
Prerequisites:
    In order to run this pipeline you need the following things:
    Install taiyaki
    activate the taiyaki env [source taiyaki/venv/bin/activate]
    Install guppy
    Install minimap2
    Install Nanoplot
Usage:
    snakemake --cores all --snakefile Snakefile
"""

BASECALL_DIRS = ["basecalls", "basecalls_new", "basecalls_final"]

rule all:
    input:
        "results/taiyaki_pipeline.png",
        expand("results/Nanoplot_output_{sample}", sample = BASECALL_DIRS),
        "results/trainings_plot.png",
        "results/mapped_signals_plot.png",
        "results/qscore_calibration.png"


#------------------------------------------------------------------<
## Step 1: Basecall the reads
rule basecall:
    """Basecall the fast5 reads with the Guppy basecaller from Oxford Nanopore Technologies."""
    input:
        samples = config["training_samples"], # fast5 sample dir
        config_model = config["config_model"],
        basecaller = config["guppy_basecaller"]
    output:
        dir = directory("basecalls"), # output dir
        file = temp(touch("basecalls.done"))
    message:
        "Basecalling reads from: {input.samples} with model: {input.config_model}. Ouput stored in output:{output.dir}"
    shell:
        "{input.basecaller} -i {input.samples} -s {output.dir} -c {input.config_model} --device cuda:1"


#------------------------------------------------------------------<
## Step 2: Mapping the basecalled reads
rule map_basecall:
    """Map the basecalled reads against the reference genome. [DWV genome.fasta from NCBI]"""
    input:
        minimap = config["minimap2"],
        reference = config["ref"],
        dir = rules.basecall.output.dir,
    output:
        "basecalls.bam"
    message:
        "Map the basecalled reads against the reference."
    shell:
        "{input.minimap} -ax splice -uf -k14 -t 32 {input.reference}*.fasta {input.dir}/*.fastq | samtools view -b -S -T {input.reference} - > {output}"
       

#------------------------------------------------------------------<
## Step 3: Extracting the references from the basecalled reads
rule extract_references:
    """Extract the references per read."""
    input:
        reference = config["ref"],
        map = rules.map_basecall.output
    output:
        temp("read_references.fasta")
    message:
        "Extracting references from the basecalled reads to output: {output}"
    shell:
        "/home/tools/taiyaki/bin/get_refs_from_sam.py {input.reference}*.fasta {input.map} --reverse --min_coverage 0.8 > {output}" # Add reverse when working with RNA

#------------------------------------------------------------------<
## Step 4: Create scaling parameters per read
rule create_scaling_params:
    """Create scaling parameters per read for the training."""
    input:
        samples = config["training_samples"], # sample dir
    output:
        temp("read_params.tsv")
    message:
        "Create scaling params per read"
    shell:
        "/home/tools/taiyaki/bin/generate_per_read_params.py --jobs 32 {input.samples} > {output}"



#------------------------------------------------------------------<
## Step 5: Create a read map file
rule create_mapped_read_file:
    """Create a mapped read file. This file contains the extracted references and the parameters per read."""
    input:
        samples = config["training_samples"],
        param = rules.create_scaling_params.output,
        read_ref = rules.extract_references.output,
        model = config["mapping_model"]
    output:
        file = "mapped_reads.hdf5"
    message:
        "Create a mapped read file align with {input.model}"
    shell:
        "/home/tools/taiyaki/bin/prepare_mapped_reads.py --jobs 32 {input.samples} {input.param} {output} {input.model} {input.read_ref}"

#------------------------------------------------------------------<
## Step 6: Train a new model
rule train_model:
    """Train the model based on the mapped read file."""
    input:
        model = config["base_model"],
        reads = rules.create_mapped_read_file.output.file
    output:
        flag = temp(touch("training_model.done")),
        dir = directory("training")
    message:
        "Training a new model for guppy with {input.model}"

    shell:
        "/home/tools/taiyaki/bin/train_flipflop.py --device cuda:1 --overwrite --stride 10 --winlen 31 --seed 2 {input.model} {input.reads} --outdir {output.dir} && cp {output.dir}/model.log results/"


#------------------------------------------------------------------<
## Step 7: Export model to guppy (json)
rule export_to_guppy:
    """Convert the .checkpoint model file into a .jsn file to be used by Guppy."""
    input:
        rules.train_model.output.flag
    output:
        "pwd/model.json"
    message:
        "Export model:{input} to json for guppy. output: {output}"
    shell:
        "/home/tools/taiyaki/bin/dump_json.py training/model_final.checkpoint > {output} && cp {output} results/"


#------------------------------------------------------------------<
## Step 8: Basecall with new model
rule basecall_with_new_model:
    """Basecall with the new model."""
    input:
        samples = config[training_samples], # fast5 sample dir
        config_model = config["config_model"],
        new_model = rules.export_to_guppy.output,
        basecaller = config["guppy_basecaller"]
    output:
        dir = directory("basecalls_new"),  # output dir
        flag = temp(touch("basecalls_new.done"))
    message:
        "Basecalling reads from: {input.samples} with model: {input.new_model}. Ouput stored in output:{output.dir}"
    shell:
        "{input.basecaller} -i {input.samples} -s {output.dir} -c {input.config_model} -m {input.new_model} --device cuda:1"

#------------------------------------------------------------------<
rule quality_check:
    """Create NanoPlot plots before and after the training. These plots show the average read length and quality of the reads."""
    input:
        basecall_dir = "{sample}",
        nanoplot = config["nanoplot"],
        info = "{sample}.done"
    output:
        directory("results/Nanoplot_output_{sample}")# output dir
    message:
        "Checking the quality of the {input.basecall_dir} reads with nanoplot after with {input.nanoplot}"
    shell:
        "{input.nanoplot} --summary {input.basecall_dir}/sequencing_summary.txt --loglength -o {output}"

#------------------------------------------------------------------<
rule workflow_dag:
    """Create a picture of the snakemake workflow"""
    output:
        "results/taiyaki_pipeline.png"
    message:
        "Creating a diagram of the workflow excecution with dot. {output}"
    shell:
        "snakemake --dag | dot -Tpng > {output}"

#------------------------------------------------------------------<
rule plot_training:
    """Plot the trainings run with a taiyaki script."""
    input:
        dir = rules.train_model.output.dir,
    output:
        "results/trainings_plot.png"
    message:
        "Plot the trainings run with the plot_training.py taiyaki script"
    shell:
        "plot_training.py {output} {input.dir} --mav 10"

#------------------------------------------------------------------<
rule plot_mapped_reads:
    """Plot the reads from the mapped signal file."""
    input:
        file = rules.create_mapped_read_file.output.file,
    output:
        "results/mapped_signals_plot.png"
    message:
        "Plot the reads from the mapped singal file."
    shell:
        "/home/tools/taiyaki/misc/plot_mapped_signals.py {input.file} --nreads 100 --maxlegendsize 10 --output {output}"

#------------------------------------------------------------------<
rule align_with_guppy:
    input:
        aligner = config["guppy_aligner"],
        dir = rules.basecall_with_new_model.output.dir,
        ref = config["ref"],
    output:
        dir = directory("guppy_alignment"),
        flag = temp(touch("guppy_align.done"))
    message:
        "Align the reads basecalled with the new model to the reference {input.ref} with the guppy aligner"
    shell:
        "{input.aligner} -i {input.dir} -s {output.dir} --align_ref {input.ref}*.fasta && cp {output.dir}/alignment_summary.txt {input.dir}"


#------------------------------------------------------------------<
rule calibrate_qscore:
    input:
        dir = rules.basecall_with_new_model.output.dir,
        flag = rules.align_with_guppy.output.flag
    output:
        "results/qscore_calibration.png"
    log: 
        "logs/qscores.log"
    message:
        "Calibrate the qscores for the new model from {input} save to {output}"
    shell:
        "/home/tools/taiyaki/misc/calibrate_qscores_byread.py --input_directory {input.dir} --plot_filename {output} 1> {log}"

#------------------------------------------------------------------<
rule basecall_final:
    """Basecall with the new model and calibrated qscores. Get the scores from the output log of the calibrate_qscore rule."""
    input:
        samples = config["test_samples"], # fast5 sample dir
        config_model = config["config_model"],
        new_model = rules.export_to_guppy.output,
        basecaller = config["guppy_basecaller"],
        logfile = rules.calibrate_qscore.log[0]
    output:
        dir = directory("basecalls_final"),  # output dir
        flag = temp(touch("basecalls_final.done"))
    message:
        "Basecalling reads from: {input.samples} with model: {input.new_model}. Ouput stored in output:{output.dir}"
    run:
        """Get the qscore slope and shift from the log output"""
        with open(input.logfile, "r") as f:
            lines = f.readlines()

            for i in lines:
                if i.startswith("Best-fit slope"):
                    linelist = i.split(" ")
                    scale = linelist[4].rstrip()
                    print("qsore_scale = ", scale)
                if i.startswith("Best-fit shift"):
                    linelist = i.split(" ")
                    offset = linelist[4].rstrip()
                    print("qscore_offset = ", offset)

        shell("{input.basecaller} -i {input.samples} -s {output.dir} -c {input.config_model} --qscore_offset {offset} --qscore_scale {scale} -m {input.new_model} --device cuda:1")
        