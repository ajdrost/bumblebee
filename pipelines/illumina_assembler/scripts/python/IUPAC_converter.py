#!/usr/bin/env python
import sys
import random
from argparse import ArgumentParser
import os.path
from os import path

#todo docstring and clean up
class IUPAC_converter:
    IUPAC_codes = {'M': ['A', 'C'], 'R': ['A', 'G'], 'W': ['A', 'U'],
                   'S': ['C', 'G'], 'Y': ['C', 'U'], 'K': ['G', 'U'],
                   'V': ['A', 'C', 'G'], 'H': ['A', 'C', 'U'],
                   'D': ['A', 'G', 'U'], 'B': ['C', 'G', 'U'],
                   'N': ['G', 'A', 'U', 'C']}

    def __init__(self, input_file_path, output_file_path):
        #TODO clearer error handeling for the user on these assertions
        assert path.exists(input_file_path), "Inputfile doesn't exists!"
        assert not path.exists(output_file_path), "Outputfile already exists!"
        self.input_file_path = input_file_path
        self.output_file_path = output_file_path

    def get_nuc(self, code):
        if code in self.IUPAC_codes.keys():
            return self.IUPAC_codes[code][random.randint(1, len(self.IUPAC_codes[code])-1)]
        elif code == "T":
            return "U"
        else:
            return code

    def procces_line(self, line):
        if line.startswith(">"):
            return line
        else:
            nucs=[self.get_nuc(nuc.capitalize()) for nuc in line]
            # converts list to string without [,]
            return ''.join(map(str, nucs))

    def convert(self):
        input_f = open(self.input_file_path, "r")
        output_f = open(self.output_file_path, "a+")
        for line in input_f:
            output_f.write(self.procces_line(line))

def main():
    parser = ArgumentParser("Tool for converting IUPAC codes into their random nucleotides")
    parser.add_argument('-i', '--input', required=True,
                        help="Path to input fasta file with IUPAC code")
    parser.add_argument('-o', '--output', required=True,
                        help="Path to output fasta file to create with IUPAC codes converted")
    args = parser.parse_args()

    converter = IUPAC_converter(input_file_path=args.input, output_file_path=args.output)
    converter.convert()

if __name__ == '__main__':
    main()