#!/usr/bin/env python
import sys
from argparse import ArgumentParser
import os.path
from os import path
import shutil

#todo docstring and clean up
class Nucleotide_converter:
    def __init__(self, input_file, output_file, type_is_dna):
        #TODO clearer error handeling for the user on these assertions
        assert path.exists(input_file), "Input file: {} doesn't exists!".format(input_file)
        assert not path.exists(output_file), "Output file : {} already exists!".format(output_file)
        self.input_file_path = input_file
        self.output_file_path = output_file
        self.type_is_dna = type_is_dna

    def convert(self):
        input_file = open(self.input_file_path, "r")
        output_file = open(self.output_file_path, "a+")
        print("converting file: {} to dna at: {}".format(self.input_file_path, self.output_file_path))

        if self.type_is_dna:
            nuc = "T"
            replacing_nuc = "U"
        else:
            nuc = "U"
            replacing_nuc = "T"

        for i, line in enumerate(input_file):
            if not (i+3) % 4:
                output_file.write(line.upper().replace(nuc, replacing_nuc))
            else:
                output_file.write(line)
            if not i % 100000:
                print("converted {} lines".format(i))

        output_file.close()
        input_file.close()
        print("Done \nSaved converted file at:{}".format(self.output_file_path))


def main():
    parser = ArgumentParser("Tool for converting fastq files to DNA or RNA")
    parser.add_argument('-i', '--input', required=True,
                        help="Input fastq file")
    parser.add_argument('-o', '--output', required=True,
                        help="output fastq file")
    parser.add_argument('--rna', help='convert a DNA fastq file to RNA',
                        action='store_true')
    parser.add_argument('--dna', help='convert a RNA fastq file to DNA',
                        action='store_true')
    args = parser.parse_args()

    if args.dna and args.rna:
        parser.print_help()
        print("\nCan't process the file because both the \"--rna\" flag and the \"--dna\" are provided.")
    elif not args.dna and not args.rna:
        parser.print_help()
        print("\nCan't process the file because neither the flag: \"--rna\" or \"--dna\" is provided.")
    else:
        Converter = Nucleotide_converter(args.input, args.output, args.dna)
        Converter.convert()

if __name__ == '__main__':
    main()