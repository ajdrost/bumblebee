args = commandArgs(trailingOnly=TRUE)
data_with_gaps <- read.csv(args[1] , header = T, sep = ",")
jpeg(args[2])

smoothingSpline = smooth.spline(data_with_gaps$Position, data_with_gaps$coverage, spar=0.4)
len <- max(data_with_gaps["Position"])

plot(data_with_gaps$coverage~data_with_gaps$Position, pch=".",ylim=c(0,130),xlim=c(0,len), col=rgb(red = 1, green = 0, blue = 0, alpha = 0.6), xlab="Position on sequence(s)", ylab="percentage coverage",yaxt="none",xaxt="none", main="Coverage per position on sequence(s) with gaps")

axis(2, seq(0,100,10),col.axis = 'white')
axis(1, seq(0,len,500),col.axis = rgb(red = 0, green = 1, blue = 1, alpha = 0.0))
axis(side=1,at=seq(0,len, 1000),labels=seq(0,len, 1000), las=2, cex.axis=1)

text(y = c(0,10,20,30,40,50,60,70,80,90,100), par("usr")[1], labels = c("0","10","20","30","40","50","60","70","80","90", "100"), pos = 2, xpd = TRUE)


rect(par("usr")[1],par("usr")[3],par("usr")[2],par("usr")[4],col = rgb(red = 0, green = 1, blue = 1, alpha = 0.1))

lines(smoothingSpline$y)

legend(0,133, legend=c("Coverage per position", "General coverage"),col=c(rgb(red = 1, green = 0, blue = 0, alpha = 0.6), "black"),  pch=c(19,NA), lwd=c(NA,3), cex=1)

dev.off()