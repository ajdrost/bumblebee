# Minor Project: Basecalling deformed wing virus data in docker
 
> This repository provides instructions on how to run docker with taiyaki pipeline.
 
## Table of contents
1. Introduction
2. Dependencies
3. Installation
4. Directory structure
5. Usage & Features
6. Building the image
7. The pipelines
8. Future plans
 
## Introduction
---
 Normally you would install all the necessary packages and install it yourself, this quite time consuming and we made it easier for you. 
 We made a docker image that has all the necessary packages and virtual environments installed to make your life easier so that you can focus on other important things. 
 Below is an instruction on how to execute the image and what you need in order to run and basecall your data. 
 One thing we need to mention is that these pipelines are set up so that only RNA data can be processed, we are looking for ways to make pipelines with DNA options as well.

## Dependencies
---
- A computer with GPU is necessary for training with taiyaki.
 
## Installation
---
Before you can use the dockerfile, make sure to install docker on your device. see [Docker](https://docs.docker.com/engine/install/ubuntu/) for more information on how to install it.


### Directory structure
---
```sh
# Tree respiratory of directory's
DockerFiles/
├── beevirus
│   ├── consensus
│   │   ├── reports
│   │   └── scripts
│   └── taiyaki
│       ├── basecall_pipeline
│       └── trainings_pipeline
├── data
│   ├── msa
│   ├── pretrained_models
│   ├── reference_genome
│   ├── sample_data
│   └── training_data
└── requirements
```

 
## Usage and Features
---
After installing docker the next thing to do is building the image, but before you do that, I recommend that you insert the sample files you wish to analyse/basecall first in the appropriate directories.
We already have some samples ready for you to test out. 
But if you wish to use your own data, please remove the samples and data except for the pretrained_models, unless you want to use your own pretrained model of course. Otherwise, the model could end up bad because of mixed samples! 

If you have single .fast5 or multi .fast5 output from MinIon, divide these in a training set and a validation set. 
Recommended ratios: 60/40 ~ 90/10. After dividing the samples, you can place them in the directory; ***/data/training_samples*** and ***/data/sample_data*** .

Taiyaki requires a reference genome to map the reads against. If you don’t have a reference genome yet, you can make one using the Consensus pipeline.
If you already have a Reference genome you can skip the Consensus pipeline and place the reference genome inside ***/data/reference_genome***.

If you dont have a reference genome and wish to make one, we have made a special pipeline just for that. 
the only requirement needed for this is a multi fasta file containing sequences of a specie you wish to make a consensus of.
place this file inside ***/data/msa/*** .

## Building the image
---
Clone the repository in a desired pathway, navigate in it and use the following command to build it, this may take a while depending on your computer:
 
```sh
# Execute docker file
docker build . -t bees/dwv:v1.0
# for more build options type ‘docker build --help’
```
 
After building the image, you will see the image #id. If not type
 
```sh
# show images
docker images
```
 
If the dockerfile build without error the new image should appear above.
To enter your new image, use the following;
 
```sh
# run image
docker run --gpus all -it <image_id>
```
 
## The pipelines
---
#### The Taiyaki Pipeline
-
Once you have run the image you will need to activate the special Taiyaki environment, this is needed to run Taiyaki.
For more information about the pipelines, I recommend that you read these instructions [Taiyaki]( https://bitbucket.org/jfhaaijer/beevirus1/src/development/)
 
```sh
# Execute Taiyaki environment
source /home/tools/taiyaki/venv/bin/activate
```
 
Now navigate to the Taiyaki pipeline ***/home/beevirus/taiyaki***, to start basecalling and train your new model. This may take a day depending on the samples and your computer. Use the following code to execute the pipeline;
 
```sh
# Execute taiyaki pipeline
snakemake --cores all
# --cores all specifies the number of cores to use, in this case, all the cores are used.
```
 
#### The consensus Pipeline
-
 
The best way to validate your taiyaki for errors is to use a consensus sequence as the reference sequence.or if you don’t have a reference sequence you can make one out of multiple sequences at once. This may take quite some time depending on how long and many sequences you wish to create a consensus of.

To execute the program, navigate to ***/home/beevirus/consensus***, then type:
 
```sh
# Execute taiyaki pipeline
snakemake --cores all
# --cores all specifies the number of cores to use, in this case, all the cores are used.
```

### Future plans
---
 
All the settings of the pipeline are oriented for RNA basecalling. It would be nice to make a new pipeline specially made for DNA basecalling. 
Make some use of docker volume that will overwrite directories in the image, so that users can easily move files, instead of putting it in the directory's by hand.


