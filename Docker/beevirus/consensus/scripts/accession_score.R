args = commandArgs(trailingOnly=TRUE)
data <- read.csv(args[1] , header = T, sep = ",")
jpeg(args[2])

data <-  data[order(-data$Gaps),]
frequency <- data$Gaps
max <- max(unlist(frequency))
tot <- max(frequency)
tot2 <- sum(frequency)
ma2 <- max(data$threshold)
max1 <- max(c(max, ma2))

total_seq <- as.integer(args[3]) *0.05

coverage_percentage <- data$accession

bf <- barplot(frequency, main="Gaps score per sequence from a Multiple Sequence Alignment (MSA)",cex.main=1, xlab="", ylab="",  names.arg=coverage_percentage,cex.names=0.7,cex.axis=0.9, mgp=c(1,1,0),ylim = c(0,max1+(0.1*max1)),border="black",las=2,space=c(1), col=as.character(data$color))
legend("topright", inset=.02,  legend=c("Above threshold", "Under threshold", "Threshold"),bg=rgb(red = 0, green = 0, blue = 1, alpha = 0.6), col=c("red", "green", rgb(red = 0, green = 0, blue = 1, alpha = 0.6)),  pch=c(15,15,NA), lwd=c(NA,NA,3), cex=1)
points(x = bf, y=data$threshold, pch="_", col=rgb(red = 0, green = 0, blue = 1, alpha = 0.6),cex=2)
rect(par("usr")[1],par("usr")[3],par("usr")[2],par("usr")[4],col =  rgb(red = 0, green = 1, blue = 1, alpha = 0.1))
text(bf+0.5, frequency, frequency,cex=0.7,pos=3, srt=45)
# x axis
mtext(text = "Accession numbers",
      side = 1,line = 4)

# y axis
mtext(text = "Gap score",
      side = 2, #side 2 = left
      line = 3, cex=1)

dev.off()